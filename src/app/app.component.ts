import {Component, ViewEncapsulation} from '@angular/core';
import {Elevator} from './model/elevator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'elevator';
  elevator: Elevator;

  constructor() {
    this.elevator = new Elevator;

  }

  onClick() {
    this.elevator.externalButtons[5].addCommand();
  }

  getcurrentCommands() {
    return this.elevator.getCmdManager().getCycle();
  }

  getQueue() {
    return this.elevator.getCmdManager().getQueue();
  }

  onDispatch() {
    this.elevator.runDispatcher();
  }
}
