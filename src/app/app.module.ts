import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ElCommandsListComponent } from './el-commands-list/el-commands-list.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { CurrentStateComponent } from './current-state/current-state.component';

@NgModule({
  declarations: [
    AppComponent,
    ElCommandsListComponent,
    ButtonsComponent,
    CurrentStateComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
