import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-current-state',
  templateUrl: './current-state.component.html',
  styleUrls: ['./current-state.component.css']
})
export class CurrentStateComponent implements OnInit {
  @Input() state;

  constructor() {
  }

  ngOnInit() {
  }

}
