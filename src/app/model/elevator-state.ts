import {ElevatorStateType} from './elevator-state-type.enum';

export class ElevatorState {
  public state: ElevatorStateType;
  floor: number;

  constructor(state: ElevatorStateType = ElevatorStateType.COMPLETED, floor: number = 0) {
    this.state = state;
    this.floor = floor;
  }

  clone(state = this.state, floor = this.floor): ElevatorState {
    return new ElevatorState(state, floor);
  }
}
