export class ElevatorCommand {
  floor: number;
  isOutside: boolean;

  constructor(floor, isOutside) {
    this.floor = floor;
    this.isOutside = isOutside;
  }

  static toString() {
    return 'Floor: ${$this.floor}, isOutside: ${this.isOutside} ';
  }
}
