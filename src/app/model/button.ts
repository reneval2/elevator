import {ElevatorCommand} from './elevator-command';

export class Button {
  floor: number;
  isOutside: boolean;
  addCommandCallback: Function;

  constructor(floor, isOutside, addCommandCallback: Function) {
    this.floor = floor;
    this.isOutside = isOutside;
    this.addCommandCallback = addCommandCallback;
  }

  public addCommand() {
    return this.addCommandCallback(new ElevatorCommand(this.floor, this.isOutside));
  }
}
