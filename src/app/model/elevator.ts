import {CommandManager} from './command-manager';
import {CommandDispatcher} from './command-dispatcher';
import {Button} from './button';
import {ElevatorCommand} from './elevator-command';
import {ElevatorState} from './elevator-state';
import {ElevatorStateType} from './elevator-state-type.enum';

export class Elevator {
  private MAX_FLOOR = 13;
  private commandManager: CommandManager;
  private commandDispatcher: CommandDispatcher;
  public internalButtons: Button[];
  public externalButtons: Button[];
  public currentState: ElevatorState;

  constructor() {
    this.commandManager = new CommandManager();
    this.currentState = new ElevatorState(ElevatorStateType.COMPLETED, 0);
    this.commandDispatcher = new CommandDispatcher(this.commandManager, this.getState);
    this.internalButtons = Array.from(Array(this.MAX_FLOOR).keys()).map(i => {
      return new Button(i, false, this.addCommad.bind(this));
    });

    this.externalButtons = Array.from(Array(this.MAX_FLOOR).keys()).map(i => {
      return new Button(i, true, this.addCommad.bind(this));
    });
  }

  addCommad(cmd: ElevatorCommand) {
    this.commandManager.addCommand(cmd, this.currentState);
  }

  getState() {
    return this.currentState;
  }
  runDispatcher() {
    // do {
       this.currentState = this.commandDispatcher.dispatch(this.currentState);
    console.log("this.currentState ::", this.currentState );
    // } while (this.currentState.state !== ElevatorStateType.COMPLETED);
  }



  getCmdManager() {
    return this.commandManager;
  }
}
