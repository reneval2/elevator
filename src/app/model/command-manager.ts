import {ElevatorCommand} from './elevator-command';
import {ElevatorState} from './elevator-state';
import {state} from '@angular/animations';
import {ElevatorStateType} from './elevator-state-type.enum';

enum Direction {
  UP = 'UP',
  DOWN = 'DOWN',
  NONE = 'NONE'
}

export class CommandManager {
  currentCycle: ElevatorCommand[] = [];
  commandQueue: ElevatorCommand[] = [];

  addCommand(cmd: ElevatorCommand, currentState: ElevatorState) {
    if (this.canAddToCurrentCycle(cmd, currentState)) {
      this.addToCurrentCycle(cmd, currentState);
    } else {
      this.commandQueue.push(cmd);
    }
  }

  canAddToCurrentCycle(cmd: ElevatorCommand, currentState: ElevatorState): boolean {
    if (currentState.state === ElevatorStateType.COMPLETED) {
      return false;
    }
    const direction = this.getCurrentDirection(currentState);
    console.log("direction", direction)
    console.log("cmd.floor", cmd.floor)
    console.log(" currentState.floor",  currentState.floor)
    return (direction === Direction.UP || cmd.floor < currentState.floor);
  }

  getNextCommand(currentState: ElevatorState) {
    if (this.currentCycle.length > 0) {
      return this.currentCycle[0];
    } else {
      if (this.commandQueue.length > 0){
        this.createCurrentCycle(currentState);
        return this.currentCycle[0];
      }
    }
  }

  getCurrentDirection(currentState: ElevatorState): Direction {
    const nextCommand = this.getNextCommand(currentState);
    if (nextCommand) {
      if (nextCommand.floor > currentState.floor) {
        return Direction.UP;
      } else {
        return Direction.DOWN;
      }
    } else {
      return Direction.NONE;
    }
  }

  private addToCurrentCycle(cmd: ElevatorCommand, currentState: ElevatorState) {
    this.currentCycle.push(cmd);
    this.currentCycle = this.reorderCommands(this.currentCycle, currentState);
  }

  public reorderCommands(commands: ElevatorCommand[], currentState: ElevatorState): ElevatorCommand[] {
    return commands.sort((a, b) => {
        if (this.isFirstCyclePart(a, currentState)) {
          if (!this.isFirstCyclePart(b, currentState)) {
            return -1;
          } else {
            return a.floor - b.floor;
          }
        } else {
          if (this.isFirstCyclePart(b, currentState)) {
            return 1;
          } else {
            return b.floor - a.floor;
          }
        }
      }
    );
  }

  private isFirstCyclePart(cmd: ElevatorCommand, currentState: ElevatorState): boolean {
    return !(cmd.isOutside || cmd.floor < currentState.floor);
  }

  public createCurrentCycle(currentState: ElevatorState) {
    this.currentCycle = this.reorderCommands(this.commandQueue, currentState);
    this.commandQueue = [];
  }

  execute() {
    const cmd = this.currentCycle[0];
    this.currentCycle = this.currentCycle.filter(i => i.floor !== cmd.floor);
  }

  getQueue() {
    return this.commandQueue;
  }

  getCycle() {
    return this.currentCycle;
  }
}
