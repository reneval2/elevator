import {CommandManager} from './command-manager';
import {ElevatorCommand} from './elevator-command';
import {ElevatorState} from './elevator-state';
import {MovementControl} from './movement-control';
import {ElevatorStateType} from './elevator-state-type.enum';

export class CommandDispatcher {
  private movementControl: MovementControl;
  private commandManager: CommandManager;
  private stateReceiver: Function;

  constructor(commandManager: CommandManager, stateReceiver: Function) {
    this.commandManager = commandManager;
    this.stateReceiver = stateReceiver;
    this.movementControl = new MovementControl();
  }

  public dispatch(currentState: ElevatorState): ElevatorState {
    const cmd: ElevatorCommand = this.commandManager.getNextCommand(currentState);
    console.log('dispatch cmd', cmd);
    if (!cmd) {
      return currentState.clone(ElevatorStateType.COMPLETED);
    }
    if (currentState.floor === cmd.floor) {
      this.commandManager.execute();
      if (this.commandManager.getNextCommand(currentState)) {
        return currentState.clone(ElevatorStateType.STOPPED);
      } else {
        return currentState.clone(ElevatorStateType.COMPLETED);
      }
    } else {
      if (currentState.floor > cmd.floor) {
        this.movementControl.moveDown();
        return currentState.clone(ElevatorStateType.MOVING_DOWN, currentState.floor - 1);
      } else {
        const res = currentState.clone(ElevatorStateType.MOVING_UP, currentState.floor + 1);
        console.log('RES::', res);
        return res;
      }
    }
  }
}
