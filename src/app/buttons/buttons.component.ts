import {Component, Input, OnInit} from '@angular/core';
import {ElevatorCommand} from '../model/elevator-command';
import {Button} from '../model/button';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.css']
})
export class ButtonsComponent implements OnInit {
  @Input() public int: Button[];
  @Input() public ext: Button[];
  constructor() { }

  ngOnInit() {
  }


}
