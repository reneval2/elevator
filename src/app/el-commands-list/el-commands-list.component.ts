import {Component, Input, OnInit} from '@angular/core';
import {ElevatorCommand} from '../model/elevator-command';

@Component({
  selector: 'app-el-commands-list',
  templateUrl: './el-commands-list.component.html',
  styleUrls: ['./el-commands-list.component.css']
})
export class ElCommandsListComponent implements OnInit {
  @Input() public commands: ElevatorCommand[];
  @Input() public title = 'Command list';

  constructor() {
  }

  ngOnInit() {
  }

  onClick() {
    console.log(this.commands);
  }
}
